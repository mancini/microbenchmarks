macro (add_precompile_target)
    set(options "")
    set(oneValueArgs TARGET_NAME)
    set(multiValueArgs SOURCES COMPILER_FLAGS INCLUDE_DIRS)
    cmake_parse_arguments(apt "${options}" "${oneValueArgs}"
                          "${multiValueArgs}" ${ARGN} )

    list(TRANSFORM apt_INCLUDE_DIRS PREPEND -I)

    add_custom_command(OUTPUT ${apt_TARGET_NAME}.s
                    COMMAND ${CMAKE_CXX_COMPILER} -std=c++${CMAKE_CXX_STANDARD} ${CMAKE_CXX_COMPLERS_ARG} ${apt_COMPILER_FLAGS} ${apt_SOURCES} ${apt_INCLUDE_DIRS} -S -fverbose-asm -Wa,aslh -o ${apt_TARGET_NAME}.s
                    DEPENDS ${apt_SOURCES}
                    )
    
    add_custom_target(precompile_${apt_TARGET_NAME} ALL
                  DEPENDS ${apt_TARGET_NAME}.s
                  COMMENT "Adding precompile target ${KERNEL_NAME} with source ${KERNEL_SOURCE}")
    set_property(
        TARGET precompile_${apt_TARGET_NAME}
        APPEND
        PROPERTY ADDITIONAL_CLEAN_FILES ${apt_TARGET_NAME}.s)
endmacro()
