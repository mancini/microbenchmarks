
#include "hermitian_square.h"

#if __AVX__
#include <immintrin.h>
inline double reduce_single(const double col[4]) {
  __m256d vec = _mm256_load_pd(col);
  __m128d low = _mm256_extractf128_pd(vec, 0);
  __m128d high = _mm256_extractf128_pd(vec, 1);
  __m128d sum = _mm_add_pd(low, high);
  sum = _mm_hadd_pd(sum, sum);
  return _mm_cvtsd_f64(sum);
}
#else
inline double reduce_single(const double col[4]) {
  double sum = col[0];
#pragma unroll
  for (size_t i = 0; i < 4; i++) {
    sum += col[i];
  }
  return sum;
}
#endif

aocommon::HMatrix4x4 HermitianSquareNaiveSIMDv2(
    const aocommon::Matrix4x4& mat) {
  const double* mat_pointer = reinterpret_cast<const double*>(&mat[0]);
  double tmp_real_col0[4];
  double tmp_real_col1[4];
  double tmp_real_col2[4];
  double tmp_real_col3[4];

  double tmp_imag_col0[4];
  double tmp_imag_col1[4];
  double tmp_imag_col2[4];
  double tmp_imag_col3[4];

  double c1c0_r[4], c2c1_r[4], c2c0_r[4], c3c2_r[4], c3c1_r[4], c3c0_r[4];
  double c1c0_i[4], c2c1_i[4], c2c0_i[4], c3c2_i[4], c3c1_i[4], c3c0_i[4];
  double tmp_real_uf[6] = {0., 0., 0., 0., 0., 0.};
  double tmp_imag_uf[6] = {0., 0., 0., 0., 0., 0.};
  double tmp_diag[4] = {0., 0., 0., 0.};

  memset(tmp_diag, 0, 4 * sizeof(double));

#pragma omp simd
  for (int k = 0; k < 4; k++) {
    tmp_real_col0[k] = mat_pointer[2 * (4 * k + 0)];
    tmp_real_col1[k] = mat_pointer[2 * (4 * k + 1)];
    tmp_real_col2[k] = mat_pointer[2 * (4 * k + 2)];
    tmp_real_col3[k] = mat_pointer[2 * (4 * k + 3)];

    tmp_imag_col0[k] = mat_pointer[2 * (4 * k + 0) + 1];
    tmp_imag_col1[k] = mat_pointer[2 * (4 * k + 1) + 1];
    tmp_imag_col2[k] = mat_pointer[2 * (4 * k + 2) + 1];
    tmp_imag_col3[k] = mat_pointer[2 * (4 * k + 3) + 1];
  }

#pragma omp simd
  for (int k = 0; k < 4; k++) {
    tmp_diag[0] += ((tmp_real_col0[k] * tmp_real_col0[k]) +
                    (tmp_imag_col0[k] * tmp_imag_col0[k]));

    tmp_diag[1] += ((tmp_real_col1[k] * tmp_real_col1[k]) +
                    (tmp_imag_col1[k] * tmp_imag_col1[k]));

    tmp_diag[2] += ((tmp_real_col2[k] * tmp_real_col2[k]) +
                    (tmp_imag_col2[k] * tmp_imag_col2[k]));

    tmp_diag[3] += ((tmp_real_col3[k] * tmp_real_col3[k]) +
                    (tmp_imag_col3[k] * tmp_imag_col3[k]));
  }
#pragma omp simd
  for (int k = 0; k < 4; k++) {
    c1c0_r[k] = tmp_real_col1[k] * tmp_real_col0[k] +
                tmp_imag_col1[k] * tmp_imag_col0[k];
    c2c0_r[k] = tmp_real_col2[k] * tmp_real_col0[k] +
                tmp_imag_col2[k] * tmp_imag_col0[k];
    c2c1_r[k] = tmp_real_col2[k] * tmp_real_col1[k] +
                tmp_imag_col2[k] * tmp_imag_col1[k];
    c3c0_r[k] = tmp_real_col3[k] * tmp_real_col0[k] +
                tmp_imag_col3[k] * tmp_imag_col0[k];
    c3c1_r[k] = tmp_real_col3[k] * tmp_real_col1[k] +
                tmp_imag_col3[k] * tmp_imag_col1[k];
    c3c2_r[k] = tmp_real_col3[k] * tmp_real_col2[k] +
                tmp_imag_col3[k] * tmp_imag_col2[k];

    c1c0_i[k] = tmp_real_col1[k] * tmp_imag_col0[k] -
                tmp_imag_col1[k] * tmp_real_col0[k];
    c2c0_i[k] = tmp_real_col2[k] * tmp_imag_col0[k] -
                tmp_imag_col2[k] * tmp_real_col0[k];
    c2c1_i[k] = tmp_real_col2[k] * tmp_imag_col1[k] -
                tmp_imag_col2[k] * tmp_real_col1[k];
    c3c0_i[k] = tmp_real_col3[k] * tmp_imag_col0[k] -
                tmp_imag_col3[k] * tmp_real_col0[k];
    c3c1_i[k] = tmp_real_col3[k] * tmp_imag_col1[k] -
                tmp_imag_col3[k] * tmp_real_col1[k];
    c3c2_i[k] = tmp_real_col3[k] * tmp_imag_col2[k] -
                tmp_imag_col3[k] * tmp_real_col2[k];
  }

  tmp_real_uf[0] = reduce_single(c1c0_r);
  tmp_real_uf[1] = reduce_single(c2c0_r);
  tmp_real_uf[2] = reduce_single(c2c1_r);
  tmp_real_uf[3] = reduce_single(c3c0_r);
  tmp_real_uf[4] = reduce_single(c3c1_r);
  tmp_real_uf[5] = reduce_single(c3c2_r);

  tmp_imag_uf[0] = reduce_single(c1c0_i);
  tmp_imag_uf[1] = reduce_single(c2c0_i);
  tmp_imag_uf[2] = reduce_single(c2c1_i);
  tmp_imag_uf[3] = reduce_single(c3c0_i);
  tmp_imag_uf[4] = reduce_single(c3c1_i);
  tmp_imag_uf[5] = reduce_single(c3c2_i);

  return {
      tmp_diag[0],
      0.0,
      0.0,
      0.0,
      {tmp_real_uf[0], tmp_imag_uf[0]},
      tmp_diag[1],
      0.0,
      0.0,
      {tmp_real_uf[1], tmp_imag_uf[1]},
      {tmp_real_uf[2], tmp_imag_uf[2]},
      tmp_diag[2],
      0.0,
      {tmp_real_uf[3], tmp_imag_uf[3]},
      {tmp_real_uf[4], tmp_imag_uf[4]},
      {tmp_real_uf[5], tmp_imag_uf[5]},
      tmp_diag[3],
  };
}