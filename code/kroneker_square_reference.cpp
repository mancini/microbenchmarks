#include "kronecker_square.h"

aocommon::HMC4x4 KroneckerSquareReference(aocommon::MC2x2& left,
                                          aocommon::MC2x2& right) {
  return aocommon::HMC4x4::KroneckerProduct(left.HermitianSquare().Transpose(),
                                            right.HermitianSquare());
}