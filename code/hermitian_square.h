#ifndef HERMITIAN_SQUARE_H
#define HERMITIAN_SQUARE_H

#include <aocommon/hmatrix4x4.h>
#include <aocommon/matrix4x4.h>

#include <iostream>
#include <random>

inline void Initialize(aocommon::Matrix4x4& mat) {
  // Initialize matrices with random complex values
  std::seed_seq seed({42});
  std::mt19937 gen(seed);
  std::uniform_real_distribution<double> dis(-1.0, 1.0);

  for (int i = 0; i < 16; i++) {
    mat[i] = std::complex<double>(dis(gen), dis(gen));
  }
}

aocommon::HMatrix4x4 HermitianSquare(const aocommon::Matrix4x4& mat);
aocommon::HMatrix4x4 HermitianSquareRefactored(const aocommon::Matrix4x4& mat);
aocommon::HMatrix4x4 HermitianSquareNaiveSIMD(const aocommon::Matrix4x4& mat);
aocommon::HMatrix4x4 HermitianSquareNaiveSIMDv2(const aocommon::Matrix4x4& mat);

#endif