#include "matrix_multiplication.h"

template <bool FMA, bool AVX2>
void matrixMultiplyReference(const std::complex<float>* a,
                             const std::complex<float>* b,
                             std::complex<float>* c) {
#if defined(__AVX__)
  float* a_ptr = reinterpret_cast<float*>(const_cast<std::complex<float>*>(a));
  float* b_ptr = reinterpret_cast<float*>(const_cast<std::complex<float>*>(b));
  float* c_ptr = reinterpret_cast<float*>(c);

  __m256 a_m = _mm256_loadu_ps(a_ptr);
  __m256 b_m = _mm256_loadu_ps(b_ptr);

  __m256 a_1 = _mm256_permute_ps(a_m, _MM_SHUFFLE(0, 0, 0, 0));
  __m256 a_2 = _mm256_permute_ps(a_m, _MM_SHUFFLE(2, 2, 2, 2));
  __m256 a_3 = _mm256_permute_ps(a_m, _MM_SHUFFLE(1, 1, 1, 1));
  __m256 a_4 = _mm256_permute_ps(a_m, _MM_SHUFFLE(3, 3, 3, 3));

#if AVX2
  __m256 b_1 =
      _mm256_permutevar8x32_ps(b_m, _mm256_set_epi32(3, 2, 1, 0, 3, 2, 1, 0));
  __m256 b_2 =
      _mm256_permutevar8x32_ps(b_m, _mm256_set_epi32(7, 6, 5, 4, 7, 6, 5, 4));
#else
  __m128 b_m_lower = _mm256_castps256_ps128(b_m);    // Extract lower 128 bits
  __m128 b_m_upper = _mm256_extractf128_ps(b_m, 1);  // Extract upper 128 bits

  __m256 b_1 = _mm256_insertf128_ps(_mm256_castps128_ps256(b_m_lower),
                                    b_m_lower, 1);  // Lower half repeated
  __m256 b_2 = _mm256_insertf128_ps(_mm256_castps128_ps256(b_m_upper),
                                    b_m_upper, 1);  // Upper half repeated
#endif

  __m256 b_3 = _mm256_permute_ps(b_1, _MM_SHUFFLE(2, 3, 0, 1));
  __m256 b_4 = _mm256_permute_ps(b_2, _MM_SHUFFLE(2, 3, 0, 1));

#if FMA
  __m256 c_p1 = _mm256_fmaddsub_ps(a_1, b_1, _mm256_mul_ps(a_3, b_3));
  __m256 c_p2 = _mm256_fmaddsub_ps(a_2, b_2, _mm256_mul_ps(a_4, b_4));
#else
  __m256 c_p1 =
      _mm256_addsub_ps(_mm256_mul_ps(a_1, b_1), _mm256_mul_ps(a_3, b_3));
  __m256 c_p2 =
      _mm256_addsub_ps(_mm256_mul_ps(a_2, b_2), _mm256_mul_ps(a_4, b_4));
#endif
  __m256 c_m = _mm256_add_ps(c_p1, c_p2);

  _mm256_storeu_ps(c_ptr, c_m);
#endif
}

void matrixMultiplyAVX(const std::complex<float>* a,
                       const std::complex<float>* b, std::complex<float>* c) {
  matrixMultiplyReference<false, false>(a, b, c);
}

void matrixMultiplyAVXFMA(const std::complex<float>* a,
                          const std::complex<float>* b,
                          std::complex<float>* c) {
  matrixMultiplyReference<true, false>(a, b, c);
}

void matrixMultiplyAVX2(const std::complex<float>* a,
                        const std::complex<float>* b, std::complex<float>* c) {
  matrixMultiplyReference<true, true>(a, b, c);
}