#include <complex>

#if defined(__ARM_NEON) || defined(__ARM_NEON__)
#define NEON_AVAILABLE

#include <arm_neon.h>
#endif

#include <iostream>

void matrixMultiplyNEONa(const std::complex<float>* a,
                         const std::complex<float>* b, std::complex<float>* c) {
#ifdef NEON_AVAILABLE
  float32_t* a_ptr =
      reinterpret_cast<float32_t*>(const_cast<std::complex<float>*>(a));
  float32_t* b_ptr =
      reinterpret_cast<float32_t*>(const_cast<std::complex<float>*>(b));
  float32_t* c_ptr = reinterpret_cast<float32_t*>(c);

  float32x2x4_t a_v = vld4_f32(a_ptr);
  float32x2x4_t b_v = vld4_f32(b_ptr);
  float32x2x4_t c_v;

  const float32x2_t a_v1_r = a_v.val[0];
  const float32x2_t a_v1_i = a_v.val[1];
  const float32x2_t a_v2_r = a_v.val[2];
  const float32x2_t a_v2_i = a_v.val[3];

  const float32x2_t b_v1_r = b_v.val[0];
  const float32x2_t b_v1_i = b_v.val[1];
  const float32x2_t b_v2_r = b_v.val[2];
  const float32x2_t b_v2_i = b_v.val[3];

  float32x2_t c_v1_r = vmov_n_f32(0);
  float32x2_t c_v2_r = vmov_n_f32(0);
  float32x2_t c_v1_i = vmov_n_f32(0);
  float32x2_t c_v2_i = vmov_n_f32(0);

  c_v1_r = vfma_lane_f32(c_v1_r, a_v1_r, b_v1_r, 0);
  c_v.val[0] = vfma_lane_f32(c_v1_r, a_v2_r, b_v1_r, 1);
  c_v2_r = vfma_lane_f32(c_v2_r, a_v1_r, b_v2_r, 0);
  c_v.val[2] = vfma_lane_f32(c_v2_r, a_v2_r, b_v2_r, 1);

  c_v1_r = vmul_lane_f32(a_v1_i, b_v1_i, 0);
  c_v1_r = vfma_lane_f32(c_v1_r, a_v2_i, b_v1_i, 1);
  c_v2_r = vmul_lane_f32(a_v1_i, b_v2_i, 0);
  c_v2_r = vfma_lane_f32(c_v2_r, a_v2_i, b_v2_i, 1);

  c_v.val[0] = vsub_f32(c_v.val[0], c_v1_r);
  c_v.val[2] = vsub_f32(c_v.val[2], c_v2_r);

  c_v1_i = vfma_lane_f32(c_v1_i, a_v1_r, b_v1_i, 0);
  c_v1_i = vfma_lane_f32(c_v1_i, a_v2_r, b_v1_i, 1);
  c_v2_i = vfma_lane_f32(c_v2_i, a_v1_r, b_v2_i, 0);
  c_v2_i = vfma_lane_f32(c_v2_i, a_v2_r, b_v2_i, 1);

  c_v1_i = vfma_lane_f32(c_v1_i, a_v1_i, b_v1_r, 0);
  c_v2_i = vfma_lane_f32(c_v2_i, a_v1_i, b_v2_r, 0);

  c_v.val[1] = vfma_lane_f32(c_v1_i, a_v2_i, b_v1_r, 1);
  c_v.val[3] = vfma_lane_f32(c_v2_i, a_v2_i, b_v2_r, 1);

  vst4_f32(c_ptr, c_v);
#endif
}

void matrixMultiplyNEONb(const std::complex<float>* a,
                         const std::complex<float>* b, std::complex<float>* c) {
#ifdef NEON_AVAILABLE
  float32_t* a_ptr =
      reinterpret_cast<float32_t*>(const_cast<std::complex<float>*>(a));
  float32_t* b_ptr =
      reinterpret_cast<float32_t*>(const_cast<std::complex<float>*>(b));
  float32_t* c_ptr = reinterpret_cast<float32_t*>(c);

  float32x4x2_t a_v = vld2q_f32(a_ptr);
  float32x4x2_t b_v = vld2q_f32(b_ptr);
  float32x4x2_t c_v;

  // To multiply two 2x2 matrices A and B:
  //
  // A = | 0 1 |    B = | 0 1 |
  //     | 2 3 |        | 2 3 |
  //
  // The product matrix C = A * B is calculated as:
  // C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0]
  // C[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1]
  // C[1][0] = A[1][0]*B[0][0] + A[1][1]*B[1][0]
  // C[1][1] = A[1][0]*B[0][1] + A[1][1]*B[1][1]
  //
  // If you consider A, B and C as 1D arrays, this becomes:
  // C[0] = A[0]*B[0] + A[1]*B[2]
  // C[1] = A[0]*B[1] + A[1]*B[3]
  // C[2] = A[2]*B[0] + A[3]*B[2]
  // C[3] = A[2]*B[1] + A[3]*B[3]

  // Multiply real
  const float32x4x2_t ta_r = vzipq_f32(a_v.val[0], a_v.val[0]);
  const float32x4x2_t ta_i = vzipq_f32(a_v.val[1], a_v.val[1]);

  const float32x4_t a0_r =
      vcombine_f32(vget_low_f32(ta_r.val[0]), vget_low_f32(ta_r.val[1]));
  const float32x4_t a0_i =
      vcombine_f32(vget_low_f32(ta_i.val[0]), vget_low_f32(ta_i.val[1]));

  const float32x4_t a1_r =
      vcombine_f32(vget_high_f32(ta_r.val[0]), vget_high_f32(ta_r.val[1]));
  const float32x4_t a1_i =
      vcombine_f32(vget_high_f32(ta_i.val[0]), vget_high_f32(ta_i.val[1]));

  const float32x2_t hb0_r = vget_high_f32(b_v.val[0]);
  const float32x2_t lb0_r = vget_low_f32(b_v.val[0]);
  const float32x2_t hb0_i = vget_high_f32(b_v.val[1]);
  const float32x2_t lb0_i = vget_low_f32(b_v.val[1]);
  const float32x4_t b0_r = vcombine_f32(lb0_r, lb0_r);
  const float32x4_t b1_r = vcombine_f32(hb0_r, hb0_r);

  const float32x4_t b0_i = vcombine_f32(lb0_i, lb0_i);
  const float32x4_t b1_i = vcombine_f32(hb0_i, hb0_i);

  float32x4_t c_rr = vmulq_f32(a0_r, b0_r);
  c_rr = vfmaq_f32(c_rr, a1_r, b1_r);

  float32x4_t c_ii = vmulq_f32(a0_i, b0_i);
  c_ii = vfmaq_f32(c_ii, a1_i, b1_i);

  float32x4_t c_ri = vmulq_f32(a0_r, b0_i);
  c_ri = vfmaq_f32(c_ri, a1_r, b1_i);

  float32x4_t c_ir = vmulq_f32(a0_i, b0_r);
  c_ir = vfmaq_f32(c_ir, a1_i, b1_r);

  c_v.val[0] = vsubq_f32(c_rr, c_ii);
  c_v.val[1] = vaddq_f32(c_ri, c_ir);

  vst2q_f32(c_ptr, c_v);
#endif
}