#ifdef __SSE__
#include <immintrin.h>

#include "matrix_multiplication.h"

struct Matrix2x2 {
  float m[4];
};

void sse_2x2_multiply_add_v1(const Matrix2x2 a, const Matrix2x2 b,
                             Matrix2x2& c) {
  // Load matrices into SSE registers
  __m128 ma = _mm_loadu_ps(a.m);
  __m128 mb = _mm_loadu_ps(b.m);
  __m128 mc = _mm_loadu_ps(c.m);

  __m128 col_a_0 = _mm_moveldup_ps(ma);
  __m128 col_a_1 = _mm_movehdup_ps(ma);
  __m128 row_b_0 = _mm_shuffle_ps(mb, mb, _MM_SHUFFLE(1, 0, 1, 0));
  __m128 row_b_1 = _mm_shuffle_ps(mb, mb, _MM_SHUFFLE(3, 2, 3, 2));

  __m128 col0 = _mm_mul_ps(col_a_0, row_b_0);
  __m128 col1 = _mm_mul_ps(col_a_1, row_b_1);

  // Horizontal add to get final results
  __m128 sum = _mm_add_ps(col0, col1);
  __m128 result = _mm_add_ps(mc, sum);
  // Store the result
  _mm_storeu_ps(c.m, result);
}

void sse_2x2_multiply_sub_v1(const Matrix2x2 a, const Matrix2x2 b,
                             Matrix2x2& c) {
  // Load matrices into SSE registers
  __m128 ma = _mm_loadu_ps(a.m);
  __m128 mb = _mm_loadu_ps(b.m);
  __m128 mc = _mm_loadu_ps(c.m);

  __m128 col_a_0 = _mm_moveldup_ps(ma);
  __m128 col_a_1 = _mm_movehdup_ps(ma);
  __m128 row_b_0 = _mm_shuffle_ps(mb, mb, _MM_SHUFFLE(1, 0, 1, 0));
  __m128 row_b_1 = _mm_shuffle_ps(mb, mb, _MM_SHUFFLE(3, 2, 3, 2));

  __m128 col0 = _mm_mul_ps(col_a_0, row_b_0);
  __m128 col1 = _mm_mul_ps(col_a_1, row_b_1);

  // Horizontal add to get final results
  __m128 sum = _mm_add_ps(col0, col1);

  __m128 result = _mm_sub_ps(mc, sum);
  // Store the result
  _mm_storeu_ps(c.m, result);
}

void matrixMultiplySSE(const std::complex<float>* a,
                       const std::complex<float>* b, std::complex<float>* c) {
  const Matrix2x2 a_real{a[0].real(), a[1].real(), a[2].real(), a[3].real()};
  const Matrix2x2 b_real{b[0].real(), b[1].real(), b[2].real(), b[3].real()};
  const Matrix2x2 a_imag{a[0].imag(), a[1].imag(), a[2].imag(), a[3].imag()};
  const Matrix2x2 b_imag{b[0].imag(), b[1].imag(), b[2].imag(), b[3].imag()};
  Matrix2x2 c_real{0.f, 0.f, 0.f, 0.f};
  Matrix2x2 c_imag{0.f, 0.f, 0.f, 0.f};

  sse_2x2_multiply_add_v1(a_real, b_real, c_real);
  sse_2x2_multiply_sub_v1(a_imag, b_imag, c_real);

  sse_2x2_multiply_add_v1(a_real, b_imag, c_imag);
  sse_2x2_multiply_add_v1(a_imag, b_real, c_imag);

  c[0] = {c_real.m[0], c_imag.m[0]};
  c[1] = {c_real.m[1], c_imag.m[1]};
  c[2] = {c_real.m[2], c_imag.m[2]};
  c[3] = {c_real.m[3], c_imag.m[3]};
}

#endif