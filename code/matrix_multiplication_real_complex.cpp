#include "matrix_multiplication.h"

inline void multiply_mat(const float* a, const float* b, float* c, float sign) {
  c[0] += sign * (a[0] * b[0] + a[1] * b[2]);
  c[1] += sign * (a[0] * b[1] + a[1] * b[3]);
  c[2] += sign * (a[2] * b[0] + a[3] * b[2]);
  c[3] += sign * (a[2] * b[1] + a[3] * b[3]);
}

void matrixMultiplyRealComplex(const std::complex<float>* a,
                               const std::complex<float>* b,
                               std::complex<float>* c) {
  const float a_real[] = {a[0].real(), a[1].real(), a[2].real(), a[3].real()};
  const float b_real[] = {b[0].real(), b[1].real(), b[2].real(), b[3].real()};
  const float a_imag[] = {a[0].imag(), a[1].imag(), a[2].imag(), a[3].imag()};
  const float b_imag[] = {b[0].imag(), b[1].imag(), b[2].imag(), b[3].imag()};

  float c_real[4] = {0, 0, 0, 0};
  float c_imag[4] = {0, 0, 0, 0};

  multiply_mat(a_real, b_real, c_real, 1.0f);
  multiply_mat(a_imag, b_imag, c_real, -1.0f);
  multiply_mat(a_real, b_imag, c_imag, 1.0f);
  multiply_mat(a_imag, b_real, c_imag, 1.0f);

  c[0] = {c_real[0], c_imag[0]};
  c[1] = {c_real[1], c_imag[1]};
  c[2] = {c_real[2], c_imag[2]};
  c[3] = {c_real[3], c_imag[3]};
}
