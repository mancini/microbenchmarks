#include "hermitian_square.h"

aocommon::HMatrix4x4 HermitianSquareNaiveSIMD(const aocommon::Matrix4x4& mat) {
  const double* mat_pointer = reinterpret_cast<const double*>(&mat[0]);
  double tmp_real_col0[4];
  double tmp_real_col1[4];
  double tmp_real_col2[4];
  double tmp_real_col3[4];

  double tmp_imag_col0[4];
  double tmp_imag_col1[4];
  double tmp_imag_col2[4];
  double tmp_imag_col3[4];

  double c1c0_r[4], c2c1_r[4], c2c0_r[4], c3c2_r[4], c3c1_r[4], c3c0_r[4];
  double c1c0_i[4], c2c1_i[4], c2c0_i[4], c3c2_i[4], c3c1_i[4], c3c0_i[4];
  double tmp_real_uf[6] = {0, 0, 0, 0, 0, 0};
  double tmp_imag_uf[6] = {0, 0, 0, 0, 0, 0};
  double tmp_diag[4];

  memset(tmp_diag, 0, 4 * sizeof(double));

#pragma omp simd
  for (int k = 0; k < 4; k++) {
    tmp_real_col0[k] = mat_pointer[2 * (4 * k + 0)];
    tmp_real_col1[k] = mat_pointer[2 * (4 * k + 1)];
    tmp_real_col2[k] = mat_pointer[2 * (4 * k + 2)];
    tmp_real_col3[k] = mat_pointer[2 * (4 * k + 3)];

    tmp_imag_col0[k] = mat_pointer[2 * (4 * k + 0) + 1];
    tmp_imag_col1[k] = mat_pointer[2 * (4 * k + 1) + 1];
    tmp_imag_col2[k] = mat_pointer[2 * (4 * k + 2) + 1];
    tmp_imag_col3[k] = mat_pointer[2 * (4 * k + 3) + 1];
  }

#pragma omp simd
  for (int k = 0; k < 4; k++) {
    tmp_diag[0] += ((tmp_real_col0[k] * tmp_real_col0[k]) +
                    (tmp_imag_col0[k] * tmp_imag_col0[k]));

    tmp_diag[1] += ((tmp_real_col1[k] * tmp_real_col1[k]) +
                    (tmp_imag_col1[k] * tmp_imag_col1[k]));

    tmp_diag[2] += ((tmp_real_col2[k] * tmp_real_col2[k]) +
                    (tmp_imag_col2[k] * tmp_imag_col2[k]));

    tmp_diag[3] += ((tmp_real_col3[k] * tmp_real_col3[k]) +
                    (tmp_imag_col3[k] * tmp_imag_col3[k]));
  }

#pragma omp simd
  for (int k = 0; k < 4; k++) {
    c1c0_r[k] = tmp_real_col1[k] * tmp_real_col0[k] +
                tmp_imag_col1[k] * tmp_imag_col0[k];
    c2c0_r[k] = tmp_real_col2[k] * tmp_real_col0[k] +
                tmp_imag_col2[k] * tmp_imag_col0[k];
    c2c1_r[k] = tmp_real_col2[k] * tmp_real_col1[k] +
                tmp_imag_col2[k] * tmp_imag_col1[k];
    c3c0_r[k] = tmp_real_col3[k] * tmp_real_col0[k] +
                tmp_imag_col3[k] * tmp_imag_col0[k];
    c3c1_r[k] = tmp_real_col3[k] * tmp_real_col1[k] +
                tmp_imag_col3[k] * tmp_imag_col1[k];
    c3c2_r[k] = tmp_real_col3[k] * tmp_real_col2[k] +
                tmp_imag_col3[k] * tmp_imag_col2[k];

    c1c0_i[k] = tmp_real_col1[k] * tmp_imag_col0[k] -
                tmp_imag_col1[k] * tmp_real_col0[k];
    c2c0_i[k] = tmp_real_col2[k] * tmp_imag_col0[k] -
                tmp_imag_col2[k] * tmp_real_col0[k];
    c2c1_i[k] = tmp_real_col2[k] * tmp_imag_col1[k] -
                tmp_imag_col2[k] * tmp_real_col1[k];
    c3c0_i[k] = tmp_real_col3[k] * tmp_imag_col0[k] -
                tmp_imag_col3[k] * tmp_real_col0[k];
    c3c1_i[k] = tmp_real_col3[k] * tmp_imag_col1[k] -
                tmp_imag_col3[k] * tmp_real_col1[k];
    c3c2_i[k] = tmp_real_col3[k] * tmp_imag_col2[k] -
                tmp_imag_col3[k] * tmp_real_col2[k];
  }

  for (int k = 0; k < 4; k++) {
    tmp_real_uf[0] += c1c0_r[k];
    tmp_real_uf[1] += c2c0_r[k];
    tmp_real_uf[2] += c2c1_r[k];
    tmp_real_uf[3] += c3c0_r[k];
    tmp_real_uf[4] += c3c1_r[k];
    tmp_real_uf[5] += c3c2_r[k];

    tmp_imag_uf[0] += c1c0_i[k];
    tmp_imag_uf[1] += c2c0_i[k];
    tmp_imag_uf[2] += c2c1_i[k];
    tmp_imag_uf[3] += c3c0_i[k];
    tmp_imag_uf[4] += c3c1_i[k];
    tmp_imag_uf[5] += c3c2_i[k];
  }

  return {
      tmp_diag[0],
      0.0,
      0.0,
      0.0,
      {tmp_real_uf[0], tmp_imag_uf[0]},
      tmp_diag[1],
      0.0,
      0.0,
      {tmp_real_uf[1], tmp_imag_uf[1]},
      {tmp_real_uf[2], tmp_imag_uf[2]},
      tmp_diag[2],
      0.0,
      {tmp_real_uf[3], tmp_imag_uf[3]},
      {tmp_real_uf[4], tmp_imag_uf[4]},
      {tmp_real_uf[5], tmp_imag_uf[5]},
      tmp_diag[3],
  };
}
