
#include "kronecker_square.h"

// This code is taken from
// https://gitlab.com/aroffringa/wsclean/-/merge_requests/772

aocommon::HMC4x4 KroneckerSquareFused(aocommon::MC2x2& a, aocommon::MC2x2& b) {
  using T = const std::complex<double>;
  using RT = const double;
  using std::conj;
  using std::norm;

  // Calculate a^H a. Because the result is Hermitian, some shortcuts
  // can be made.
  RT p00 = norm(a.Get(0)) + norm(a.Get(2));
  T p01 = conj(a.Get(0)) * a.Get(1) + conj(a.Get(2)) * a.Get(3);
  RT p11 = norm(a.Get(1)) + norm(a.Get(3));

  // Calculate b^H b.
  RT q00 = norm(b.Get(0)) + norm(b.Get(2));
  T q10 = conj(b.Get(1)) * b.Get(0) + conj(b.Get(3)) * b.Get(2);  // = conj(q01)
  RT q11 = norm(b.Get(1)) + norm(b.Get(3));

  // Calculate the Kronecker product of p^T and q
  RT m00 = p00 * q00;
  T m10 = p00 * q10;
  RT m11 = p00 * q11;
  T m20 = p01 * q00;
  T m21 = p01 * conj(q10);
  RT m22 = p11 * q00;
  T m30 = p01 * q10;
  T m31 = p01 * q11;
  T m32 = p11 * q10;
  RT m33 = p11 * q11;

  return aocommon::HMC4x4::FromData(
      {m00, m10.real(), m10.imag(), m11, m20.real(), m20.imag(), m21.real(),
       m21.imag(), m22, m30.real(), m30.imag(), m31.real(), m31.imag(),
       m32.real(), m32.imag(), m33});
}
