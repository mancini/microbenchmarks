#include "matrix_multiplication.h"

void matrixMultiplyAoCommon(const std::complex<float>* a,
                            const std::complex<float>* b,
                            std::complex<float>* c) {
  const aocommon::MC2x2F A(a[0], a[1], a[2], a[3]);
  const aocommon::MC2x2F B(b[0], b[1], b[2], b[3]);

  const aocommon::MC2x2F C = A * B;
  c[0] = C.Get(0);
  c[1] = C.Get(1);
  c[2] = C.Get(2);
  c[3] = C.Get(3);
}
