#ifndef MATRIX_MULTIPLICATION_H_
#define MATRIX_MULTIPLICATION_H_
#include <aocommon/matrix2x2.h>

#include <complex>
#include <iomanip>
#include <iostream>
#include <new>  // For std::align_val_t
#include <random>

inline void Initialize(std::complex<float>* a) {
  // Initialize matrices with random complex values
  std::seed_seq seed({42});
  std::mt19937 gen(seed);
  std::uniform_real_distribution<float> dis(-1.0, 1.0);

  for (int i = 0; i < 4; i++) {
    a[i] = std::complex<float>(dis(gen), dis(gen));
  }
}

// Function to perform matrix multiplication for 2x2 complex matrices
void matrixMultiplyReference(const std::complex<float>* A,
                             const std::complex<float>* B,
                             std::complex<float>* C);

void matrixMultiplyRealComplex(const std::complex<float>* a,
                               const std::complex<float>* b,
                               std::complex<float>* c);

void matrixMultiplyAoCommon(const std::complex<float>* a,
                            const std::complex<float>* b,
                            std::complex<float>* c);

void matrixMultiplyAVX(const std::complex<float>* a,
                       const std::complex<float>* b, std::complex<float>* c);

void matrixMultiplyAVXFMA(const std::complex<float>* a,
                          const std::complex<float>* b, std::complex<float>* c);

void matrixMultiplyAVX2(const std::complex<float>* a,
                        const std::complex<float>* b, std::complex<float>* c);

void matrixMultiplyNEONa(const std::complex<float>* a,
                         const std::complex<float>* b, std::complex<float>* c);

void matrixMultiplyNEONb(const std::complex<float>* a,
                         const std::complex<float>* b, std::complex<float>* c);

void matrixMultiplySSE(const std::complex<float>* a,
                       const std::complex<float>* b, std::complex<float>* c);

#endif
