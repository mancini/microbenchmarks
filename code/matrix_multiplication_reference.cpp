#include "matrix_multiplication.h"

void matrixMultiplyReference(const std::complex<float>* A,
                             const std::complex<float>* B,
                             std::complex<float>* C) {
  for (int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      std::complex<float> sum = 0.0f;
      for (int k = 0; k < 2; ++k) {
        sum += A[i * 2 + k] * B[k * 2 + j];
      }
      C[i * 2 + j] = sum;
    }
  }
}