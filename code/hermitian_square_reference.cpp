#include "hermitian_square.h"

aocommon::HMatrix4x4 HermitianSquare(const aocommon::Matrix4x4& mat) {
  return mat.HermitianSquare();
}

template <typename T>
using cmplx = std::complex<T>;

aocommon::HMatrix4x4 HermitianSquareRefactored(const aocommon::Matrix4x4& mat) {
  auto N = [](std::complex<double> z) -> double { return std::norm(z); };

  /*
    Mat is a complex matrix indexed as
     0  1  2  3
     4  5  6  7
     8  9 10 11
    12 13 14 15

    norm = r*r + i*i #ops = 3
    conj = r +ji -> r - ji #ops = 0
    complex multiplication (CM) = (a + ib ) * (c + i d )
    -> a*c - b*d + i (a d +  bc) -> 4 mult 1 sum 1 sub -> #ops 6
    complex conjugate multiplication (CCM) = (a - ib ) * (c + i d )
    -> a*c + b*d + i (-bc + ad) -> 4 mult 1 sum 1 sub -> #ops 6


    On the diagonal there are 4 norms + 3 addition -> #ops 15
    there are 4 values in the diagonal so 60 operations

    Off diagonal 4 complex multiplication -> #ops 24
    24 * 6 off diagonal element #ops 144

    144 + 60 -> 204 operations per matrix
  */

  const cmplx<double> R00 = std::norm(mat[0]) + std::norm(mat[4]) +
                            std::norm(mat[8]) + std::norm(mat[12]);
  const cmplx<double> R01 = 0.0;
  const cmplx<double> R02 = 0.0;
  const cmplx<double> R03 = 0.0;

  const cmplx<double> R10 =
      std::conj(mat[1]) * mat[0] + std::conj(mat[5]) * mat[4] +
      std::conj(mat[9]) * mat[8] + std::conj(mat[13]) * mat[12];
  const cmplx<double> R11 = std::norm(mat[1]) + std::norm(mat[5]) +
                            std::norm(mat[9]) + std::norm(mat[13]);
  const cmplx<double> R12 = 0.0;
  const cmplx<double> R13 = 0.0;

  const cmplx<double> R20 =
      std::conj(mat[2]) * mat[0] + std::conj(mat[6]) * mat[4] +
      std::conj(mat[10]) * mat[8] + std::conj(mat[14]) * mat[12];
  const cmplx<double> R21 =
      std::conj(mat[2]) * mat[1] + std::conj(mat[6]) * mat[5] +
      std::conj(mat[10]) * mat[9] + std::conj(mat[14]) * mat[13];
  const cmplx<double> R22 = std::norm(mat[2]) + std::norm(mat[6]) +
                            std::norm(mat[10]) + std::norm(mat[14]);
  const cmplx<double> R23 = 0.0;

  const cmplx<double> R30 =
      std::conj(mat[3]) * mat[0] + std::conj(mat[7]) * mat[4] +
      std::conj(mat[11]) * mat[8] + std::conj(mat[15]) * mat[12];
  const cmplx<double> R31 =
      std::conj(mat[3]) * mat[1] + std::conj(mat[7]) * mat[5] +
      std::conj(mat[11]) * mat[9] + std::conj(mat[15]) * mat[13];
  const cmplx<double> R32 =
      std::conj(mat[3]) * mat[2] + std::conj(mat[7]) * mat[6] +
      std::conj(mat[11]) * mat[10] + std::conj(mat[15]) * mat[14];
  const cmplx<double> R33 = std::norm(mat[3]) + std::norm(mat[7]) +
                            std::norm(mat[11]) + std::norm(mat[15]);

  return {R00, R01, R02, R03, R10, R11, R12, R13,
          R20, R21, R22, R23, R30, R31, R32, R33};
}
