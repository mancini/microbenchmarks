#ifndef KRONECKER_SQUARE_H
#define KRONECKER_SQUARE_H

#include <aocommon/hmatrix4x4.h>
#include <aocommon/matrix4x4.h>

#include <iostream>
#include <random>

inline void Initialize(aocommon::MC2x2& a) {
  // Initialize matrices with random complex values
  std::seed_seq seed({42});
  std::mt19937 gen(seed);
  std::uniform_real_distribution<double> dis(-1.0, 1.0);

  for (size_t i = 0; i < 4; i++) {
    a.Set(i, std::complex<double>(dis(gen), dis(gen)));
  }
}

aocommon::HMC4x4 KroneckerSquareReference(aocommon::MC2x2& left,
                                          aocommon::MC2x2& right);

aocommon::HMC4x4 KroneckerSquareFused(aocommon::MC2x2& left,
                                      aocommon::MC2x2& right);

#endif  // KRONECKER_SQUARE_H