import json
import os
from argparse import ArgumentParser
import seaborn
import pandas
import matplotlib.pyplot as plt

seaborn.set_theme(palette="bright", style="whitegrid")

def parse_args():
    parser = ArgumentParser(description="Combine benchmark metrics from Google Benchmarks Framework")
    parser.add_argument("files", nargs="+", help="Metrics")
    parser.add_argument("output", help="Combined metrics json")
    parser.add_argument("--filter", help="Filter tests by name")
    return parser.parse_args()

def read_and_combine_json(files, filter):
    results = []
    for f_name in files:
        basename = os.path.basename(f_name)
        with open(f_name, 'r') as f_stream:
            result_obj = json.load(f_stream)
            results_normalized = result_obj["benchmarks"]
            for result_normalized in results_normalized:
                if filter  and filter not in result_normalized["name"]:
                    continue
                result_normalized["context"] = result_obj["context"]
                result_normalized["compiler_version"], result_normalized["architecture"] = basename.replace("results-", "").replace(".json", "").split("-")
                results.append(result_normalized)
    return results

def store_combined(outfile, obj):
    with open(outfile + ".json", "w") as f_stream:
        json.dump(obj, f_stream, indent=4)

def create_summary_plot(metrics, outplot_name):
    time_unit = metrics.time_unit[0]
    
    grid = seaborn.FacetGrid(metrics, col="architecture")
    grid.map(seaborn.barplot, "compiler_version", "cpu_time", "name")
    grid.set_ylabels(f"CPU time({time_unit})")
    grid.add_legend()
    grid.savefig(outplot_name + ".png")

def main():
    args = parse_args()
    metrics_results = read_and_combine_json(args.files, args.filter)
    metrics_dataframe= pandas.DataFrame(metrics_results)
    create_summary_plot(metrics_dataframe, args.output)
    store_combined(args.output, metrics_results)

if __name__ == '__main__':
    main()