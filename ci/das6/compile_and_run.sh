#!/bin/bash

set -e
# compile the code and run it on das6
# Specify the compiler version and architecture

ARCHITECTURE=$1
COMPILER_VERSION=$2


echo RUNNING ON ${COMPILER_VERSION} AND ${ARCHITECTURE}
BUILD_DIR=build-${COMPILER_VERSION}-${ARCHITECTURE}
module load spack/${COMPILER_VERSION}
module load cmake

cmake -B ${BUILD_DIR} . -DCMAKE_BUILD_TYPE=Release

make -C ${BUILD_DIR} -j

tar -cvf asm-${ARCHITECTURE}-${COMPILER_VERSION}.tar ${BUILD_DIR}/*.s

${BUILD_DIR}/microbenchmarks --benchmark_out=results-${COMPILER_VERSION}-${ARCHITECTURE}.json --benchmark_out_format=json