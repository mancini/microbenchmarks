#include <benchmark/benchmark.h>
#include <kronecker_square.h>

#include <iostream>

namespace {

class InitializeInput : public benchmark::Fixture {
 public:
  void SetUp(::benchmark::State& state) {
    A = std::make_unique<aocommon::MC2x2>();
    B = std::make_unique<aocommon::MC2x2>();
    InitializeInput(*A);
    InitializeInput(*B);
  }
  void TearDown(::benchmark::State& state) {
    A.reset();
    B.reset();
  }

  std::unique_ptr<aocommon::MC2x2> A, B;
};
}  // namespace

// Reference implementation
BENCHMARK_F(InitializeInput, KroneckerSquareReference)
(benchmark::State& state) {
  for (auto _ : state) {
    aocommon::HMC4x4 r = aocommon::HMC4x4::Zero();
    for (size_t i = 0; i < 100000000; i++) {
      r += KroneckerSquareReference(*A, *B);
    }
  }
}

// Fused implementation
BENCHMARK_F(InitializeInput, KroneckerSquareFused)
(benchmark::State& state) {
  for (auto _ : state) {
    aocommon::HMC4x4 r = aocommon::HMC4x4::Zero();
    for (size_t i = 0; i < 100000000; i++) {
      r += KroneckerSquareFused(*A, *B);
        }
  }
}
