#include <benchmark/benchmark.h>
#include <matrix_multiplication.h>

class InitializeInput : public benchmark::Fixture {
 public:
  void SetUp(::benchmark::State& state) {
    A = static_cast<std::complex<float>*>(
        std::aligned_alloc(32, 4 * sizeof(std::complex<float>)));
    B = static_cast<std::complex<float>*>(
        std::aligned_alloc(32, 4 * sizeof(std::complex<float>)));
    C = static_cast<std::complex<float>*>(
        std::aligned_alloc(32, 4 * sizeof(std::complex<float>)));
    // Initialize matrices with random complex values
    Initialize(A);
    Initialize(B);
  }
  void TearDown(::benchmark::State& state) {
    // Free the allocated memory
    std::free(A);
    std::free(B);
    std::free(C);
  }

  std::complex<float>* A;
  std::complex<float>* B;
  std::complex<float>* C;
};

// Reference standard
BENCHMARK_F(InitializeInput, MatrixMultiplicationReference)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyReference(A, B, C);
  }
}

// Using naive implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationRealComplex)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyRealComplex(A, B, C);
  }
}

// Using aocommon implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationAOCommon)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyAoCommon(A, B, C);
  }
}

#if defined(__AVX__)
// Using direct avx implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationAvx)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyAVX(A, B, C);
  }
}
#endif

#if defined(__AVX__) && defined(__FMA__)
// Using direct avx+fma implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationAvxFma)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyAVXFMA(A, B, C);
  }
}
#endif

#if defined(__AVX2__)
// Using direct avx2 implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationAvx2)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyAVX2(A, B, C);
  }
}
#endif

#if defined(__ARM_NEON)
// Using direct NEON implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationNEONa)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyNEONa(A, B, C);
  }
}
#endif

#if defined(__ARM_NEON)
// Using direct NEON implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationNEONb)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplyNEONb(A, B, C);
  }
}
#endif

#if defined(__SSE__)
// Using direct NEON implementation
BENCHMARK_F(InitializeInput, MatrixMultiplicationSSE)
(benchmark::State& state) {
  for (auto _ : state) {
    matrixMultiplySSE(A, B, C);
  }
}
#endif
