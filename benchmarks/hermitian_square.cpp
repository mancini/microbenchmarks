#include <benchmark/benchmark.h>
#include <hermitian_square.h>

#include <iostream>

namespace {

class InitializeInput : public benchmark::Fixture {
 public:
  void SetUp(::benchmark::State& state) {
    A = std::make_unique<aocommon::Matrix4x4>();
    InitializeInput(*A);
  }
  void TearDown(::benchmark::State& state) { A.reset(); }

  std::unique_ptr<aocommon::Matrix4x4> A;
};
}  // namespace

// Reference standard
BENCHMARK_F(InitializeInput, HermitianSquare)
(benchmark::State& state) {
  for (auto _ : state) {
    HermitianSquare(*A);
  }
}

BENCHMARK_F(InitializeInput, HermitianSquareRefactored)
(benchmark::State& state) {
  for (auto _ : state) {
    HermitianSquareRefactored(*A);
  }
}

BENCHMARK_F(InitializeInput, HermitianSquareNaiveSIMD)
(benchmark::State& state) {
  for (auto _ : state) {
    HermitianSquareNaiveSIMD(*A);
  }
}

BENCHMARK_F(InitializeInput, HermitianSquareNaiveSIMDv2)
(benchmark::State& state) {
  for (auto _ : state) {
    HermitianSquareNaiveSIMDv2(*A);
  }
}
