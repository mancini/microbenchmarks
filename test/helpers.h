#ifndef HELPERS_H

#define HELPERS_H

#include <aocommon/hmatrix4x4.h>
#include <aocommon/matrix4x4.h>

#include <array>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <complex>
#include <string>
#include <vector>

#define COMPARE_ARRAYS(lhs, rhs, precision)                                    \
  compareArrays(Catch::getResultCapture().getCurrentTestName(), __LINE__, lhs, \
                rhs, precision)

template <typename T>
void compareSingle(const std::vector<T>& lv, const std::vector<T>& rv,
                   float precision);

template <>
void compareSingle(const std::vector<std::complex<float>>& lv,
                   const std::vector<std::complex<float>>& rv, float precision);

template <typename T>
std::string valueToString(const T& value);

std::string valueToString(const std::complex<float>& value);

template <typename T, size_t N>
void compareArrays(const std::string& test, unsigned line, std::array<T, N> lhs,
                   std::array<T, N> rhs, float precision);

void AssertEqual(const aocommon::Matrix4x4& a, const aocommon::Matrix4x4& b,
                 float precision);

void AssertEqual(const aocommon::HMatrix4x4& a, const aocommon::HMatrix4x4& b,
                 float precision);

void PrintMatrix(const aocommon::Matrix4x4& mat);

#endif  // HELPERS_H