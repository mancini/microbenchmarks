#include <matrix_multiplication.h>

#include <catch2/catch_test_macros.hpp>

#include "helpers.h"

TEST_CASE("test complex matrix multiplication", "[float]") {
  // This setup will be done 4 times in total, once for each section
  std::array<std::complex<float>, 4> A;
  std::array<std::complex<float>, 4> B;
  std::array<std::complex<float>, 4> C;

  std::array<std::complex<float>, 4> C_expected;

  Initialize(A.data());
  Initialize(B.data());

  matrixMultiplyReference(A.data(), B.data(), C_expected.data());

  SECTION("test correctness of naive implementation") {
    matrixMultiplyRealComplex(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }

  SECTION("test correctness of aocommon implementation") {
    matrixMultiplyAoCommon(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }

#if defined(__SSE__)
  SECTION("test correctness of SSE implementation") {
    matrixMultiplySSE(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }
#endif

#if defined(__AVX__)
  SECTION("test correctness of avx implementation") {
    matrixMultiplyAVX(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }
#endif

#if defined(__AVX__) && defined(__FMA__)
  SECTION("test correctness of avx+fma implementation") {
    matrixMultiplyAVXFMA(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }
#endif

#if defined(__AVX2__)
  SECTION("test correctness of avx2 implementation") {
    matrixMultiplyAVX2(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }
#endif

#if defined(__ARM_NEON)
  SECTION("test correctness of NEON implemenentation") {
    matrixMultiplyNEONa(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }
#endif

#if defined(__ARM_NEON)
  SECTION("test correctness of NEON Naive implemenentation") {
    matrixMultiplyNEONb(A.data(), B.data(), C.data());

    COMPARE_ARRAYS(C_expected, C, 1.e-6);
  }
#endif
}