#include "helpers.h"

#include <aocommon/matrix4x4.h>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>
#include <iostream>

#define COMPARE_ARRAYS(lhs, rhs, precision)                                    \
  compareArrays(Catch::getResultCapture().getCurrentTestName(), __LINE__, lhs, \
                rhs, precision)

template <typename T>
void compareSingle(const std::vector<T>& lv, const std::vector<T>& rv,
                   float precision) {
  REQUIRE_THAT(lv, Catch::Matchers::WithinAbs(rv, precision));
}

template <>
void compareSingle(const std::vector<std::complex<float>>& lv,
                   const std::vector<std::complex<float>>& rv,
                   float precision) {
  for (size_t idx = 0; idx < lv.size(); idx++) {
    const auto le = lv[idx];
    const auto re = rv[idx];

    REQUIRE_THAT(le.real(), Catch::Matchers::WithinAbs(re.real(), precision));
    REQUIRE_THAT(le.imag(), Catch::Matchers::WithinAbs(re.imag(), precision));
  }
}

template <typename T>
std::string valueToString(const T& value) {
  return std::to_string(value);
}

std::string valueToString(const std::complex<float>& value) {
  return std::to_string(value.real()) + " " + std::to_string(value.imag()) +
         "j";
}

template <typename T, size_t N>
void compareArrays(const std::string& test, unsigned line, std::array<T, N> lhs,
                   std::array<T, N> rhs, float precision) {
  std::vector<T> lv(lhs.begin(), lhs.end());
  std::vector<T> rv(rhs.begin(), rhs.end());
  INFO("Test case [" << test << "] failed at line "
                     << line);  // Reported only if REQUIRE fails

  std::stringstream ss;
  ss << "Expected : \n";
  for (size_t idx = 0; idx < N; idx++) {
    ss << valueToString(lhs[idx]) << "\t";
  }

  ss << "\nObtained : \n";
  for (size_t idx = 0; idx < N; idx++) {
    ss << valueToString(rhs[idx]) << "\t";
  }
  ss << "\n";
  INFO("Reason: \n" << ss.str());
  compareSingle(lv, rv, precision);
}

template void compareArrays(const std::string& test, unsigned line,
                            std::array<std::complex<float>, 4ul> lhs,
                            std::array<std::complex<float>, 4ul> rhs,
                            float precision);

void AssertEqual(const aocommon::Matrix4x4& a, const aocommon::Matrix4x4& b,
                 float precision) {
  for (size_t i = 0; i < 16; i++) {
    REQUIRE_THAT(a[i].real(),
                 Catch::Matchers::WithinAbs(b[i].real(), precision));
    REQUIRE_THAT(a[i].imag(),
                 Catch::Matchers::WithinAbs(b[i].imag(), precision));
  }
}

void AssertEqual(const aocommon::HMatrix4x4& a, const aocommon::HMatrix4x4& b,
                 float precision) {
  for (size_t i = 0; i < 16; i++) {
    REQUIRE_THAT(a[i].real(),
                 Catch::Matchers::WithinAbs(b[i].real(), precision));
    REQUIRE_THAT(a[i].imag(),
                 Catch::Matchers::WithinAbs(b[i].imag(), precision));
  }
}
void PrintMatrix(const aocommon::Matrix4x4& mat) {
  for (size_t j = 0; j < 4; j++) {
    for (size_t i = 0; i < 3; i++) std::cout << mat[i + j * 4] << "\t";
    std::cout << mat[3 + j * 4] << std::endl;
  }
}

void PrintMatrix(const aocommon::HMatrix4x4& mat) {
  PrintMatrix(mat.ToMatrix());
}