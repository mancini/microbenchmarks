#include <kronecker_square.h>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

#include "helpers.h"

TEST_CASE("test kronecker square matrix multiplication", "[double]") {
  // This setup will be done 4 times in total, once for each section
  aocommon::MC2x2 A;
  aocommon::MC2x2 B;
  aocommon::Matrix4x4 C;

  aocommon::Matrix4x4 C_expected;

  Initialize(A);
  Initialize(B);

  C_expected = KroneckerSquareReference(A, B).ToMatrix();

  SECTION("test correctness of fused implementation") {
    C = KroneckerSquareFused(A, B).ToMatrix();

    AssertEqual(C_expected, C, 1.e-6);
  }
}