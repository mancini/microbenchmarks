#include <hermitian_square.h>

#include <catch2/catch_test_macros.hpp>
#include <catch2/matchers/catch_matchers_floating_point.hpp>

#include "helpers.h"

TEST_CASE("test hermitian square", "[double]") {
  // This setup will be done 4 times in total, once for each section
  aocommon::Matrix4x4 A;
  aocommon::HMatrix4x4 HA;
  aocommon::HMatrix4x4 HA_expected;

  Initialize(A);

  HA_expected = HermitianSquare(A);

  SECTION("test correctness of refactored implementation") {
    HA = HermitianSquareRefactored(A);
    CHECK(HA_expected == HA);
  }

  SECTION("test correctness of nasty implementation") {
    HA = HermitianSquareNaiveSIMD(A);

    AssertEqual(HA_expected, HA, 1.e-6);
  }
}